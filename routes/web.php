<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function ()
{
	return view('usuarios.tipos.login');
});

Route::resource('usuarios', 'usuarioController');
Route::resource('canciones', 'cancionController');
Route::resource('generos', 'generoController');
Route::resource('tiposusuarios', 'tipoUserController');

Route::resource('login','loginController');

Route::get('crear_password/{clave}', function($clave){
	//dd('$clave');
	return bcrypt($clave);
});
Route::get('logout', ['as' =>'logout', 'uses' => 'loginController@Logout']);
Route::get('inicio', function () {
    return view('home.index');
});

Route::get('mi-cuenta', function () {
    return view('usuarios.tipos.index');
});

//Registro de usuario
Route::get('register', function(){
	return view('usuarios.tipos.create');
});
Route::post('register',['as' => 'register', 'uses' => 'registerController@postregister']);

//Registro oyente
Route::get('register-oyente', function(){
	return view('usuarios.tipos.oyente');
});
Route::post('register-oyente',['as' => 'register-oyente', 'uses' => 'registerController@postregisterOyente']);


// paara obtener JSon de todas las canciones
Route::get('obtenercanciones','cancionController@obtenercanciones');

// paara obtener JSon de todas los GENEROS
Route::get('obtenergeneros','generoController@obtenergeneros');
// paara obtener JSon de todas las CANCIONES por GENEROS
Route::get('obtenercancionesporgenero/{genero}','cancionController@obtenercancionesporgenero');
//json para las musicas de cada artista
Route::get('obtenercancionesporartista/{usuario}','cancionController@obtenercancionesporartista');
// paara obtener JSon de todas las ARTISTAS
Route::get('obtenerartistas/{tipo_usuario}','usuarioController@obtenerartistas');
//Descargas 
Route::get('downloadFile/{id}', 'cancionController@downloadFile');
//Route::post('comprobarUser','loginController@comprobarUser');
Route::get('busquedamovilcancion/{id}','cancionController@busquedamovilcancion');
//Route::get('logout', 'TokenAuthController@logout');
//Route::post('logout',['as' => 'login', 'uses' => 'loginController@logout']);
Route::get('comprobarlogin/{password}/{username}','loginController@comprobarlogin');
