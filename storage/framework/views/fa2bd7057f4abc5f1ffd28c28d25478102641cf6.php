<?php $__env->startSection('title', 'Actualizar cancion'); ?>

<?php $__env->startSection('content'); ?>

<?php echo $__env->make('alerts.request', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="wrapper wrapper-content animated fadeInRight">
  <?php echo Form::model($cancion, array('method' => 'PATCH', 'route' => array('canciones.update', $cancion->id))); ?>   
    <div class="form-group">
            <?php echo Form::label('titulo cancion', 'Titulo'); ?>

            <?php echo Form::Text('titulo',null,['class'=>'form-control', 'placeholder'=>'Ingrese nuevo titulo']); ?>

    </div>
    <div class="form-group">
            <?php echo Form::label('descripcion cancion', 'Descripcion'); ?>

            <?php echo Form::Text('descripcion',null,['class'=>'form-control','placeholder'=>'Ingrese nueva descripcion']); ?>

    </div>   
    <div class="form-group">
        <?php echo Form::label('genero de la cancion', 'Genero    :'); ?>

        <?php echo Form::select('genero_id', $generocancion, ['class' => 'form-control']); ?>

    </div> 
    <?php if(Auth::user()->tipo_usuario_id==1): ?> 
    <div class="form-group">
        <?php echo Form::label('compositar de la cancion', 'Artista    :'); ?>

        <?php echo Form::select('cantante_id', $artista, ['class' => 'form-control']); ?>

    </div>
    <?php endif; ?>
    <div class="form-group">
        <?php echo Form::label('cancion', 'Canción    :'); ?>

        <input type="file" name="cancion">
    </div> 
            <?php echo Form::submit('Actualizar cancion',['class'=>'btn btn-primary']); ?>

<?php echo Form::close(); ?>

</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>