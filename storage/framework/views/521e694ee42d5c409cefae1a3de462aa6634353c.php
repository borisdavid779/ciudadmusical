<!DOCTYPE html>
<html lang="en">
<head>
    <title>Crear cuenta</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="admin/login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="admin/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--==============================================================================================
    <link rel="stylesheet" type="text/css" href="admin/login/fonts/iconic/css/material-design-iconic-font.min.css">
<!-===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="admin/login/vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="admin/login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="admin/login/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="admin/login/vendor/select2/select2.min.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="admin/login/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="admin/login/css/util.css">
    <link rel="stylesheet" type="text/css" href="admin/login/css/main.css">
<!--===============================================================================================-->
</head>
<body>    
    <div class="limiter">
        <div class="container-login100" style="background-image: url('images/bg-01.jpg');">
            <div class="wrap-login100">
                <form class="login100-form validate-form" action="register" method="POST" >
                   <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <span class="login100-form-logo">
                        <i class="zmdi zmdi-landscape"></i>
                    </span>
                    <span class="login100-form-title p-b-34 p-t-27">
                        Crear tu cuenta de Ciudad Musical
                    </span>
                    <div class="wrap-input100 validate-input m-b-10" data-validate="Nombre requerido">
                        <input class="input100" type="text" name="nombre" placeholder="Nombre">
                        <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>
                    <div class="wrap-input100 validate-input m-b-10" data-validate="Apellido requerido">
                        <input class="input100" type="text" name="apellido" placeholder="Apellido">
                        <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>

                    <div class="wrap-input100 validate-input m-b-10" data-validate="Mail requerido">
                        <input class="input100" type="mail" name="mail" placeholder="Mail">
                        <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>
                    <div class="wrap-input100 validate-input m-b-10" data-validate = "Username requerido">
                        <input class="input100" type="text" name="username" placeholder="Username">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                    <div class="wrap-input100 validate-input m-b-10" data-validate="Password requerido">
                        <input class="input100" type="password" name="password" placeholder="Password">
                        <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>
                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn" method="post" action="register">
                            Registrarse
                        </button>
                    </div>
                        </div>
                    <div class="text-center">
                    <?php echo $__env->make('alerts.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('alerts.request', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('alerts.success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="dropDownSelect1"></div>
<!--===============================================================================================-->
    <script src="admin/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
    <script src="admin/login/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
    <script src="admin/login/vendor/bootstrap/js/popper.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
    <script src="admin/login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
    <script src="admin/login/vendor/daterangepicker/moment.min.js"></script>
    <script src="admin/login/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
    <script src="admin/login/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
    <script src="admin/login/js/main.js"></script>

</body>
</html>