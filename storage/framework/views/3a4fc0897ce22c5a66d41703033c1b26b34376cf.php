<?php $__env->startSection('title', 'Tipos Usuarios'); ?>

<?php $__env->startSection('content'); ?>
<?php echo $__env->make('alerts.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('alerts.success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('alerts.request', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="wrapper wrapper-content animated fadeInRight">
   <?php if(count($tipos)): ?>
    <table class=" table">
        <thead>
            <tr>
                <th>ID</th>
                <th>TIPO</th>
                <th>DESCRIPCÒN</th>
            </tr>
        </thead>
        <tbody>
            <?php $__currentLoopData = $tipos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tipo): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <tr>
                <td><?php echo e($tipo->id); ?></td>
                <td><?php echo e($tipo->tipo); ?></td>
                <td><?php echo e($tipo->descripcion); ?></td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        </tbody>
    </table>
    <?php else: ?>
    <?php endif; ?>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>