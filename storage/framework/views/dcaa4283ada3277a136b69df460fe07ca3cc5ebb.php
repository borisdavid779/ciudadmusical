<?php $__env->startSection('title', 'Generos'); ?>

<?php $__env->startSection('content'); ?>
<?php echo $__env->make('alerts.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('alerts.success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('alerts.request', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<div class="wrapper wrapper-content animated fadeInRight">
   <?php if(Auth::user()->tipo_usuario_id==1): ?>
    <a class="btn btn-primary" href="<?php echo e(ROUTE('generos.create')); ?>">NUEVO GENERO</a>
   <?php endif; ?>
    <?php if(count($generos)): ?>
   <table id="mytable" class="table table-striped table-bordered" style="width:100%" >
        <thead>
            <tr>
                <th>ID</th>
                <th>GENERO</th>
                <?php if(Auth::user()->tipo_usuario_id==1): ?>
                <th>OPCIONES</th>
                <?php endif; ?> 
            </tr>
        </thead>
        <tbody>
            <?php $__currentLoopData = $generos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $genero): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <tr>
                <td><?php echo e($genero->id); ?></td>
                <td><?php echo e($genero->genero); ?></td>
                  <td class="botones"> 
                  <?php if(Auth::user()->tipo_usuario_id==1): ?>
                   <?php echo Form::open(array('method' => 'GET', 'route' => array('generos.edit', $genero->id))); ?>   
                        <button type="submit" class="btn btn-success bnt-lg"   data-toggle="tooltip"  title="Editar">      
                        <i class="fa fa-pencil-square-o"></i></button>
                        <?php echo Form::close(); ?>

              
                   <?php echo Form::open(array('method' => 'DELETE', 'route' => array('generos.destroy', $genero->id))); ?>   
                        <button type="submit" class="btn btn-danger bnt-lg"   data-toggle="tooltip"  title="Eliminar">
                        <i class="fa fa-trash"></i></button>
                        <?php echo Form::close(); ?>

                  <?php endif; ?> 
                  </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        </tbody>
    </table>
    <?php else: ?>
    <?php endif; ?>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      
            $('#mytable').DataTable({
                responsive : true,
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                }
              
                  
            });

} );
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>