<?php $__env->startSection('title', 'Crear Usuario'); ?>

<?php $__env->startSection('content'); ?>

<?php echo $__env->make('alerts.request', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="wrapper wrapper-content animated fadeInRight">
   <?php echo Form::open(['route'=>'generos.store', 'method' => 'POST']); ?>    
    <div class="form-group">
            <?php echo Form::label('Nombre de genero', 'Nombre de genero'); ?>

            <?php echo Form::Text('genero',null,['class'=>'form-control', 'placeholder'=>'Ingrese nombre']); ?>

    </div> 

            <?php echo Form::submit('Registrar genero',['class'=>'btn btn-primary']); ?>

<?php echo Form::close(); ?>

</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>