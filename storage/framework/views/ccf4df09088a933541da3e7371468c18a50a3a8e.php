<?php $__env->startSection('title', 'Actualizar genero'); ?>

<?php $__env->startSection('content'); ?>

<?php echo $__env->make('alerts.request', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php if(Auth::user()->tipo_usuario_id==1): ?>
<div class="wrapper wrapper-content animated fadeInRight">
  <?php echo Form::model($genero, array('method' => 'PATCH', 'route' => array('generos.update', $genero->id))); ?>   
    <div class="form-group">
            <?php echo Form::label('Nombre', 'Nombre'); ?>

            <?php echo Form::Text('genero',null,['class'=>'form-control', 'placeholder'=>'Ingrese nuevo genero']); ?>

    </div>
    <div class="form-group">
            <?php echo Form::submit('Actualizar genero',['class'=>'btn btn-primary']); ?>

<?php echo Form::close(); ?>

</div>
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>