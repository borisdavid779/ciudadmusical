<?php $__env->startSection('title', 'Subir cancion'); ?>

<?php $__env->startSection('content'); ?>

<?php echo $__env->make('alerts.request', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="wrapper wrapper-content animated fadeInRight">
   <?php echo Form::open(['route'=>'canciones.store', 'type'=>'hidden','name' =>'_token','value'=> '<?php echo e(csrf_token()); ?>', 'method' => 'POST','files'=>'true']); ?>    
    <div class="form-group">
            <?php echo Form::label('titulo de cancion', 'Titulo'); ?>

            <?php echo Form::Text('titulo',null,['class'=>'form-control', 'placeholder'=>'Ingrese titulo']); ?>

    </div>
    <div class="form-group">
            <?php echo Form::label('descripcion de cancion', 'Descripcion'); ?>

            <?php echo Form::Text('descripcion',null,['class'=>'form-control','placeholder'=>'Ingrese descripcion.']); ?>

    </div>  
    <div class="form-group">
        <?php echo Form::label('genero_id', 'Genero    :'); ?>

        <?php echo Form::select('genero_id', $generocancion, ['class' => 'form-control']); ?>

    </div>
    <?php if(Auth::user()->tipo_usuario_id==1): ?>   
    <div class="form-group">
        <?php echo Form::label('cantante_id', 'Artista    :'); ?>

        <?php echo Form::select('cantante_id', $artista, ['class' => 'form-control']); ?>

    </div>  
    <?php endif; ?> 
    <div class="form-group">
        <?php echo Form::label('imagen_cancion', 'Imagen    :'); ?>

        <input type="file" name="imagen_cancion">
    </div> 
    <div class="form-group">
        <?php echo Form::label('cancion', 'Canción    :'); ?>

        <input type="file" name="cancion">
    </div>   
            <?php echo Form::submit('Registrar cancion',['class'=>'btn btn-primary']); ?>

<?php echo Form::close(); ?>

</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>