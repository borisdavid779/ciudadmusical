<?php $__env->startSection('title', 'Crear Usuario'); ?>

<?php $__env->startSection('content'); ?>

<?php echo $__env->make('alerts.request', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="wrapper wrapper-content animated fadeInRight">
   <?php echo Form::open(['route'=>'usuarios.store', 'method' => 'POST']); ?>    
    <div class="form-group">
            <?php echo Form::label('Nombre de usuario', 'Nombre '); ?>

            <?php echo Form::Text('nombre',null,['class'=>'form-control', 'placeholder'=>'Ingrese nombre']); ?>

    </div>
    <div class="form-group">
            <?php echo Form::label('apellido', 'Apellido '); ?>

            <?php echo Form::Text('apellido',null,['class'=>'form-control','placeholder'=>'Ingrese su apellido.']); ?>

    </div>  
    <div class="form-group">
            <?php echo Form::label('username', 'Nombre de usuario'); ?>

            <?php echo Form::Text('username',null,['class'=>'form-control','placeholder'=>'Ingrese username']); ?>

    </div>
    <div class="form-group">
            <?php echo Form::label('mail', 'Direccion de Mail'); ?>

            <?php echo Form::Text('mail',null,['class'=>'form-control','placeholder'=>'Ingrese mail']); ?>

    </div>     

            <?php echo Form::submit('Registrar Usuario',['class'=>'btn btn-primary']); ?>

<?php echo Form::close(); ?>

</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>