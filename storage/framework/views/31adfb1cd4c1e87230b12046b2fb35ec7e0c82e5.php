<?php $__env->startSection('title', 'Actualizar Usuario'); ?>

<?php $__env->startSection('content'); ?>

<?php echo $__env->make('alerts.request', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="wrapper wrapper-content animated fadeInRight">
  <?php echo Form::model($usuario, array('method' => 'PATCH', 'route' => array('usuarios.update', $usuario->id))); ?>   
    <div class="form-group">
            <?php echo Form::label('nombre', 'Nombre'); ?>

            <?php echo Form::Text('nombre',null,['class'=>'form-control', 'placeholder'=>'Ingrese nuevo nombre']); ?>

    </div>
    <div class="form-group">
            <?php echo Form::label('apellido', 'Apellido del usuario'); ?>

            <?php echo Form::Text('apellido',null,['class'=>'form-control','placeholder'=>'Ingrese su apellido.']); ?>

    </div>   
    <div class="form-group">
            <?php echo Form::label('mail', 'Direccion de Mail'); ?>

            <?php echo Form::Text('mail',null,['class'=>'form-control','placeholder'=>'Ingrese nuevo mail']); ?>

    </div> 
    <div class="form-group">
            <?php echo Form::label('username ', 'Nombre de usuario '); ?>

            <?php echo Form::Text('username',null,['class'=>'form-control', 'placeholder'=>'Ingrese nuevo username']); ?>

    </div>
    <div class="form-group">
        <?php echo Form::label('tipo_usuario_id', 'Sección    :'); ?>

        <?php echo Form::select('tipo_usuario_id', $tiposuser, ['class' => 'form-control']); ?>

    </div>
            <?php echo Form::submit('Actualizar Usuario',['class'=>'btn btn-primary']); ?>

<?php echo Form::close(); ?>

</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>