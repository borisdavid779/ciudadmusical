<?php $__env->startSection('title', 'Canciones'); ?>

<?php $__env->startSection('scripts-head'); ?>
  <!-- Data Tables -->

<?php $__env->startSection('content'); ?>
<?php echo $__env->make('alerts.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('alerts.success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('alerts.request', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<div class="wrapper wrapper-content animated fadeInRight">
<?php if(Auth::user()->tipo_usuario_id==1 or Auth::user()->tipo_usuario_id==2): ?>   
    <a class="btn btn-primary" href="<?php echo e(ROUTE('canciones.create')); ?>">NUEVA CANCION</a>
<?php endif; ?>
  <div class="text-center m-t-lg">
      <h1>
        Canciones 
      </h1> 
      <br>
  </div>
    <?php if(count($canciones)): ?>
    <table  id="tbbuzon" class="table table-striped table-bordered" style="width:100%" >
        <thead>
            <tr>
                <?php if(Auth::user()->tipo_usuario_id==1): ?>                
                <th>ID</th>
                <?php endif; ?>
                <th>IMAGEN</th>
                <th>TITULO</th>
                <?php if(Auth::user()->tipo_usuario_id==1 or Auth::user()->tipo_usuario_id== 3): ?>                
                <th>ARTISTA</th>
                <?php endif; ?>
                <th>GENERO</th>
                <th>DESCRIPCIÒN</th>
                <th>REPRODUCCION</th>
                <th>OPCIONES</th>

            </tr>
        </thead>
        <tbody>
            <?php $__currentLoopData = $canciones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cancion): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <tr>
                <?php if(Auth::user()->tipo_usuario_id==1): ?>
                <td><?php echo e($cancion->id); ?></td>
                <?php endif; ?> 
                <td>
                  <div class="card" style="width: 8rem;">
                      <img class="card-img-top" src="<?php echo e(asset('public/'.$cancion->imagen_cancion)); ?>" style="width:105px">
                  </div>
                </td>  
      
                <td><?php echo e($cancion->titulo); ?></td>
                
                <?php if(Auth::user()->tipo_usuario_id==1 or Auth::user()->tipo_usuario_id==3): ?>   
                <td><?php echo e($cancion->nombre." ".$cancion->apellido); ?></td>
                <?php endif; ?>
                <td><?php echo e($cancion->genero); ?></td>
                <td><?php echo e($cancion->descripcion); ?></td>
                <td class="center">
                    <?php echo $__env->make('home.repro', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </td>
                <td>
                <?php if(Auth::user()->tipo_usuario_id==1 or Auth::user()->tipo_usuario_id==2): ?>
             

                 <?php echo Form::open(array('method' => 'GET', 'route' => array('canciones.edit', $cancion->id))); ?>   
                      <button type="submit" class="btn btn-success bnt-lg"   data-toggle="tooltip"  title="Editar">
                      <i class="fa fa-pencil-square-o"></i></button>
                      <?php echo Form::close(); ?>

                
                 <?php echo Form::open(array('method' => 'DELETE', 'route' => array('canciones.destroy', $cancion->id))); ?>   
                      <button type="submit" class="btn btn-danger bnt-lg"   data-toggle="tooltip"  title="Eliminar">
                      <i class="fa fa-trash"></i></button>
                      <?php echo Form::close(); ?>

                
                <?php endif; ?> 
                
                    <span class="link">
                      <a href="<?php echo e(asset('public/'.$cancion->archivo)); ?>" title="Descargar <?php echo e($cancion->titulo); ?>" download class="btn btn-lg"><i class="fa fa-download"></i></a>
                    </span>
                </td>     
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<?php if(Auth::user()->tipo_usuario_id==3 || Auth::user()->tipo_usuario_id==2|| Auth::user()->tipo_usuario_id==1): ?> 
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      
            $('#tbbuzon').DataTable({
                responsive : true,
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                }
            });

} );
</script>
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>