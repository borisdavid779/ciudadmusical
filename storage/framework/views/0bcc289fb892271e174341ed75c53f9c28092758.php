<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="admin/login/images/icons/music.ico"/>
    <title>CM - <?php echo $__env->yieldContent('title'); ?> </title>


    <link rel="stylesheet" href="<?php echo e(asset('public/css/bootstrap.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('public/css/vendor.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('public/css/app.css')); ?>" />


    
    <!-- Mainly scripts -->
    
    <script src="<?php echo e(asset('public/js/jquery-3.3.1.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('admin/datatables/datatables.min.css')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('admin/datatables/datatables.min.js')); ?>" type="text/javascript"></script>

    <?php echo $__env->yieldContent('scripts-head'); ?>
    
</head>
<body>

  <!-- Wrapper-->
    <div id="wrapper">

        <!-- Navigation -->
        <?php echo $__env->make('layouts.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <!-- Page wraper -->
        <div id="page-wrapper" class="gray-bg">

            <!-- Page wrapper -->
            <?php echo $__env->make('layouts.topnavbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <!-- Main view  -->
                   <div class="wrapper wrapper-content animated fadeInRight">           
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5><?php echo $__env->yieldContent('title'); ?> </h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content" class="container">
                                       <?php echo $__env->yieldContent('content'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                       

            <!-- Footer -->
            <?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>
        <!-- End page wrapper-->

    </div>


<?php $__env->startSection('scripts'); ?>
<?php echo $__env->yieldSection(); ?>

</body>
</html>
