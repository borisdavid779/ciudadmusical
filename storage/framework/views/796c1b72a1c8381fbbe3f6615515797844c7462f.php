<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            
            <li class="nav-header" href="inicio">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="inicio">
                        <img src="admin/login/images/5.png" style="width: 180px;" alt="LOGO" >
                    </a>
                </div>
                <div class="logo-element">
                    CM
                </div>
            </li>
            <li class="nav-header" href="inicio">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span >
                            <span class="block m-t-xs">
                                <a href="">
                                    <?php echo e(Auth::user()->nombre." ".Auth::user()->apellido); ?>

                                </a>
                            </span>
                        </span>
                    </a>
                </div>
            </li>

                <?php if(Auth::user()->tipo_usuario_id==1): ?>
                <!-- usuario admin-->

                    <li <?php echo e((Request::is("usuarios") ? 'class=active' : '')); ?>>    
                    <a href="<?php echo e(url('usuarios')); ?>"><i class="fa fa-child "></i> <span class="nav-label">Usuarios</span></a>
                    </li>

                    <li class="<?php echo e(isActiveRoute('minor')); ?>">
                        <a href="<?php echo e(url('tiposusuarios')); ?>"><i class="fa fa-align-center "></i> <span class="nav-label">Tipo usuario</span> </a>
                    
                    </li>
                    <li class="<?php echo e(isActiveRoute('main')); ?>">
                        <a href="<?php echo e(url('generos')); ?>"><i class="fa fa-th-large"></i> <span class="nav-label">Generos</span></a>
                    </li>
                    <li <?php echo e((Request::is("canciones") ? 'class=active' : '')); ?>>   
                        <a href="<?php echo e(url('canciones')); ?>"><i class="fa fa-align-left "></i> <span class="nav-label">Canciones</span></a>
                    </li>
                <?php endif; ?>
                    <?php if(Auth::user()->tipo_usuario_id==2 or Auth::user()->tipo_usuario_id==3): ?>
                    <!-- usuario artista o oyente-->
                      <li class="<?php echo e(isActiveRoute('main')); ?>">
                            <a href="<?php echo e(url('generos')); ?>"><i class="fa fa-th-large"></i> <span class="nav-label">Generos</span></a>
                      </li>
                      <li <?php echo e((Request::is("canciones") ? 'class=active' : '')); ?>>   
                            <a href="<?php echo e(url('canciones')); ?>"><i class="fa fa-align-left "></i> <span class="nav-label">Canciones</span></a>
                      </li>
                <?php endif; ?>
        </ul>
    </div>
</nav>
