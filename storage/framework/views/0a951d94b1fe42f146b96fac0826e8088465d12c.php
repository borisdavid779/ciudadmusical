<?php $__env->startSection('title', 'Tipos Usuarios'); ?>

<?php $__env->startSection('content'); ?>
<?php echo $__env->make('alerts.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('alerts.success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('alerts.request', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <?php if(count($tipo_usuario)): ?>
    <table class=" table">
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRES</th>
                 <th>APELLIDOS</th>
                 <th>OPCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php $__currentLoopData = $usuarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuario): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <tr>
                <td><?php echo e($usuario->id); ?></td>
                <td><?php echo e($usuario->nombre); ?></td>
                <td><?php echo e($usuario->apellido); ?></td>
                <td>
                 <?php echo Form::open(array('method' => 'GET', 'route' => array('usuarios.edit', $usuario->id))); ?>   
                      <button type="submit" class="btn btn-success bnt-lg"   data-toggle="tooltip"  title="Editar">
                      <i class="fa fa-pencil-square-o"></i></button>
                      <?php echo Form::close(); ?>

                </td>
                <td>
                 <?php echo Form::open(array('method' => 'DELETE', 'route' => array('usuarios.destroy', $usuario->id))); ?>   
                      <button type="submit" class="btn btn-danger bnt-lg"   data-toggle="tooltip"  title="Eliminar">
                      <i class="fa fa-trash"></i></button>
                      <?php echo Form::close(); ?>

                </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        </tbody>
    </table>
    <?php else: ?>
    <?php endif; ?>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>