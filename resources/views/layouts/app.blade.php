<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="admin/login/images/icons/music.ico"/>
    <title>CM - @yield('title') </title>


    <link rel="stylesheet" href="{{  asset('public/css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{  asset('public/css/vendor.css') }}" />
    <link rel="stylesheet" href="{{  asset('public/css/app.css') }}" />


    {{-- <script src="{!! asset('public/js/app.js') !!}" type="text/javascript"></script>
     --}}
    <!-- Mainly scripts -->
    
    <script src="{{ asset('public/js/jquery-3.3.1.js') }}" type="text/javascript"></script>
    <script src="{{  asset('admin/datatables/datatables.min.css') }}" type="text/javascript"></script>
    <script src="{{ asset('admin/datatables/datatables.min.js') }}" type="text/javascript"></script>

    @yield('scripts-head')
    
</head>
<body>

  <!-- Wrapper-->
    <div id="wrapper">

        <!-- Navigation -->
        @include('layouts.navigation')

        <!-- Page wraper -->
        <div id="page-wrapper" class="gray-bg">

            <!-- Page wrapper -->
            @include('layouts.topnavbar')

            <!-- Main view  -->
                   <div class="wrapper wrapper-content animated fadeInRight">           
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>@yield('title') </h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content" class="container">
                                       @yield('content')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                       

            <!-- Footer -->
            @include('layouts.footer')

        </div>
        <!-- End page wrapper-->

    </div>

{{--<script src="{!! asset('js/app.js') !!}" type="text/javascript"></script--}}
@section('scripts')
@show

</body>
</html>
