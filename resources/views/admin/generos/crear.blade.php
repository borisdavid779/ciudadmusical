@extends('layouts.app')

@section('title', 'Crear Usuario')

@section('content')

@include('alerts.request')
<div class="wrapper wrapper-content animated fadeInRight">
   {!! Form::open(['route'=>'generos.store', 'method' => 'POST']) !!}    
    <div class="form-group">
            {!! Form::label('Nombre de genero', 'Nombre de genero') !!}
            {!! Form::Text('genero',null,['class'=>'form-control', 'placeholder'=>'Ingrese nombre']) !!}
    </div> 

            {!!Form::submit('Registrar genero',['class'=>'btn btn-primary'])!!}
{!! Form::close() !!}
</div>
@endsection