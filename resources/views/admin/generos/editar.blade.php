@extends('layouts.app')

@section('title', 'Actualizar genero')

@section('content')

@include('alerts.request')
@if(Auth::user()->tipo_usuario_id==1)
<div class="wrapper wrapper-content animated fadeInRight">
  {!! Form::model($genero, array('method' => 'PATCH', 'route' => array('generos.update', $genero->id))) !!}   
    <div class="form-group">
            {!! Form::label('Nombre', 'Nombre') !!}
            {!! Form::Text('genero',null,['class'=>'form-control', 'placeholder'=>'Ingrese nuevo genero']) !!}
    </div>
    <div class="form-group">
            {!!Form::submit('Actualizar genero',['class'=>'btn btn-primary'])!!}
{!! Form::close() !!}
</div>
@endif
@endsection