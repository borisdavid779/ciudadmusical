@extends('layouts.app')

@section('title', 'Generos')

@section('content')
@include('alerts.errors')
@include('alerts.success')
@include('alerts.request')
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<div class="wrapper wrapper-content animated fadeInRight">
   @if(Auth::user()->tipo_usuario_id==1)
    <a class="btn btn-primary" href="{{ROUTE('generos.create')}}">NUEVO GENERO</a>
   @endif
    @if(count($generos))
   <table id="mytable" class="table table-striped table-bordered" style="width:100%" >
        <thead>
            <tr>
                <th>ID</th>
                <th>GENERO</th>
                @if(Auth::user()->tipo_usuario_id==1)
                <th>OPCIONES</th>
                @endif 
            </tr>
        </thead>
        <tbody>
            @foreach($generos as $genero)
            <tr>
                <td>{{$genero->id}}</td>
                <td>{{$genero->genero}}</td>
                  <td class="botones"> 
                  @if(Auth::user()->tipo_usuario_id==1)
                   {!! Form::open(array('method' => 'GET', 'route' => array('generos.edit', $genero->id))) !!}   
                        <button type="submit" class="btn btn-success bnt-lg"   data-toggle="tooltip"  title="Editar">      
                        <i class="fa fa-pencil-square-o"></i></button>
                        {!! Form::close() !!}
              
                   {!! Form::open(array('method' => 'DELETE', 'route' => array('generos.destroy', $genero->id))) !!}   
                        <button type="submit" class="btn btn-danger bnt-lg"   data-toggle="tooltip"  title="Eliminar">
                        <i class="fa fa-trash"></i></button>
                        {!! Form::close() !!}
                  @endif 
                  </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    @endif
</div>
@endsection
@section('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      
            $('#mytable').DataTable({
                responsive : true,
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                }
              
                  
            });

} );
</script>
@stop
