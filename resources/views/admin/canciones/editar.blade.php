@extends('layouts.app')

@section('title', 'Actualizar cancion')

@section('content')

@include('alerts.request')
<div class="wrapper wrapper-content animated fadeInRight">
  {!! Form::model($cancion, array('method' => 'PATCH', 'route' => array('canciones.update', $cancion->id))) !!}   
    <div class="form-group">
            {!! Form::label('titulo cancion', 'Titulo') !!}
            {!! Form::Text('titulo',null,['class'=>'form-control', 'placeholder'=>'Ingrese nuevo titulo']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('descripcion cancion', 'Descripcion') !!}
            {!! Form::Text('descripcion',null,['class'=>'form-control','placeholder'=>'Ingrese nueva descripcion']) !!}
    </div>   
    <div class="form-group">
        {!! Form::label('genero de la cancion', 'Genero    :') !!}
        {!!Form::select('genero_id', $generocancion, ['class' => 'form-control'])!!}
    </div> 
    @if(Auth::user()->tipo_usuario_id==1) 
    <div class="form-group">
        {!! Form::label('compositar de la cancion', 'Artista    :') !!}
        {!!Form::select('cantante_id', $artista, ['class' => 'form-control'])!!}
    </div>
    @endif
    <div class="form-group">
        {!! Form::label('cancion', 'Canción    :') !!}
        <input type="file" name="cancion">
    </div> 
            {!!Form::submit('Actualizar cancion',['class'=>'btn btn-primary'])!!}
{!! Form::close() !!}
</div>
@endsection