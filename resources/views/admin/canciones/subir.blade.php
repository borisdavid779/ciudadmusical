@extends('layouts.app')

@section('title', 'Subir cancion')

@section('content')

@include('alerts.request')
<div class="wrapper wrapper-content animated fadeInRight">
   {!! Form::open(['route'=>'canciones.store', 'type'=>'hidden','name' =>'_token','value'=> '{{csrf_token()}}', 'method' => 'POST','files'=>'true']) !!}    
    <div class="form-group">
            {!! Form::label('titulo de cancion', 'Titulo') !!}
            {!! Form::Text('titulo',null,['class'=>'form-control', 'placeholder'=>'Ingrese titulo']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('descripcion de cancion', 'Descripcion') !!}
            {!! Form::Text('descripcion',null,['class'=>'form-control','placeholder'=>'Ingrese descripcion.']) !!}
    </div>  
    <div class="form-group">
        {!! Form::label('genero_id', 'Genero    :') !!}
        {!!Form::select('genero_id', $generocancion, ['class' => 'form-control'])!!}
    </div>
    @if(Auth::user()->tipo_usuario_id==1)   
    <div class="form-group">
        {!! Form::label('cantante_id', 'Artista    :') !!}
        {!!Form::select('cantante_id', $artista, ['class' => 'form-control'])!!}
    </div>  
    @endif 
    <div class="form-group">
        {!! Form::label('imagen_cancion', 'Imagen    :') !!}
        <input type="file" name="imagen_cancion">
    </div> 
    <div class="form-group">
        {!! Form::label('cancion', 'Canción    :') !!}
        <input type="file" name="cancion">
    </div>   
            {!!Form::submit('Registrar cancion',['class'=>'btn btn-primary'])!!}
{!! Form::close() !!}
</div>
@endsection