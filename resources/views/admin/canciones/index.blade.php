@extends('layouts.app')

@section('title', 'Canciones')

@section('scripts-head')
  <!-- Data Tables -->

@section('content')
@include('alerts.errors')
@include('alerts.success')
@include('alerts.request')
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<div class="wrapper wrapper-content animated fadeInRight">
@if(Auth::user()->tipo_usuario_id==1 or Auth::user()->tipo_usuario_id==2)   
    <a class="btn btn-primary" href="{{ROUTE('canciones.create')}}">NUEVA CANCION</a>
@endif
  <div class="text-center m-t-lg">
      <h1>
        Canciones 
      </h1> 
      <br>
  </div>
    @if(count($canciones))
    <table  id="tbbuzon" class="table table-striped table-bordered" style="width:100%" >
        <thead>
            <tr>
                @if(Auth::user()->tipo_usuario_id==1)                
                <th>ID</th>
                @endif
                <th>IMAGEN</th>
                <th>TITULO</th>
                @if(Auth::user()->tipo_usuario_id==1 or Auth::user()->tipo_usuario_id== 3)                
                <th>ARTISTA</th>
                @endif
                <th>GENERO</th>
                <th>DESCRIPCIÒN</th>
                <th>REPRODUCCION</th>
                <th>OPCIONES</th>

            </tr>
        </thead>
        <tbody>
            @foreach($canciones as $cancion)
            <tr>
                @if(Auth::user()->tipo_usuario_id==1)
                <td>{{$cancion->id}}</td>
                @endif 
                <td>
                  <div class="card" style="width: 8rem;">
                      <img class="card-img-top" src="{{ asset('public/'.$cancion->imagen_cancion)}}" style="width:105px">
                  </div>
                </td>  
      
                <td>{{$cancion->titulo}}</td>
                
                @if(Auth::user()->tipo_usuario_id==1 or Auth::user()->tipo_usuario_id==3)   
                <td>{{$cancion->nombre." ".$cancion->apellido}}</td>
                @endif
                <td>{{$cancion->genero}}</td>
                <td>{{$cancion->descripcion}}</td>
                <td class="center">
                    @include('home.repro')
                </td>
                <td>
                @if(Auth::user()->tipo_usuario_id==1 or Auth::user()->tipo_usuario_id==2)
             

                 {!! Form::open(array('method' => 'GET', 'route' => array('canciones.edit', $cancion->id))) !!}   
                      <button type="submit" class="btn btn-success bnt-lg"   data-toggle="tooltip"  title="Editar">
                      <i class="fa fa-pencil-square-o"></i></button>
                      {!! Form::close() !!}
                
                 {!! Form::open(array('method' => 'DELETE', 'route' => array('canciones.destroy', $cancion->id))) !!}   
                      <button type="submit" class="btn btn-danger bnt-lg"   data-toggle="tooltip"  title="Eliminar">
                      <i class="fa fa-trash"></i></button>
                      {!! Form::close() !!}
                
                @endif 
                
                    <span class="link">
                      <a href="{{ asset('public/'.$cancion->archivo) }}" title="Descargar {{$cancion->titulo}}" download class="btn btn-lg"><i class="fa fa-download"></i></a>
                    </span>
                </td>     
                </tr>
            @endforeach
        </tbody>
    </table>
    @endif
</div>
@endsection
@section('scripts')
@if(Auth::user()->tipo_usuario_id==3 || Auth::user()->tipo_usuario_id==2|| Auth::user()->tipo_usuario_id==1) 
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      
            $('#tbbuzon').DataTable({
                responsive : true,
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                }
            });

} );
</script>
@endif
@stop