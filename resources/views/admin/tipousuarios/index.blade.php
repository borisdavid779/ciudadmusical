@extends('layouts.app')

@section('title', 'Tipos Usuarios')

@section('content')
@include('alerts.errors')
@include('alerts.success')
@include('alerts.request')

<div class="wrapper wrapper-content animated fadeInRight">
   @if(count($tipos))
    <table class=" table">
        <thead>
            <tr>
                <th>ID</th>
                <th>TIPO</th>
                <th>DESCRIPCÒN</th>
            </tr>
        </thead>
        <tbody>
            @foreach($tipos as $tipo)
            <tr>
                <td>{{$tipo->id}}</td>
                <td>{{$tipo->tipo}}</td>
                <td>{{$tipo->descripcion}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    @endif
</div>
@endsection
