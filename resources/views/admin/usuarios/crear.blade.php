@extends('layouts.app')

@section('title', 'Crear Usuario')

@section('content')

@include('alerts.request')
<div class="wrapper wrapper-content animated fadeInRight">
    {!! Form::open(['route'=>'usuarios.store', 'method' => 'POST']) !!}    
        <div class="form-group">
                {!! Form::label('Nombre de usuario', 'Nombre ') !!}
                {!! Form::Text('nombre',null,['class'=>'form-control', 'placeholder'=>'Ingrese nombre']) !!}
        </div>
        <div class="form-group">
                {!! Form::label('apellido', 'Apellido ') !!}
                {!! Form::Text('apellido',null,['class'=>'form-control','placeholder'=>'Ingrese su apellido.']) !!}
        </div>  
        <div class="form-group">
                {!! Form::label('username', 'Nombre de usuario') !!}
                {!! Form::Text('username',null,['class'=>'form-control','placeholder'=>'Ingrese username']) !!}
        </div>
        <div class="form-group">
                {!! Form::label('mail', 'Direccion de Mail') !!}
                {!! Form::Text('mail',null,['class'=>'form-control','placeholder'=>'Ingrese mail']) !!}
        </div>   
        <div class="form-group">
            {!! Form::label('tipo_usuario_id', 'Tipo de usuario    :') !!}
            {!!Form::select('tipo_usuario_id', $tiposuser, ['class' => 'form-control'])!!}
        </div>   
            {!!Form::submit('Registrar Usuario',['class'=>'btn btn-primary'])!!}
    {!! Form::close() !!}
</div>
@endsection