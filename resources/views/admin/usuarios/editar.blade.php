@extends('layouts.app')

@section('title', 'Actualizar Usuario')

@section('content')

@include('alerts.request')
<div class="wrapper wrapper-content animated fadeInRight">
  {!! Form::model($usuario, array('method' => 'PATCH', 'route' => array('usuarios.update', $usuario->id))) !!}   
    <div class="form-group">
            {!! Form::label('nombre', 'Nombre') !!}
            {!! Form::Text('nombre',null,['class'=>'form-control', 'placeholder'=>'Ingrese nuevo nombre']) !!}
    </div>
    <div class="form-group">
            {!! Form::label('apellido', 'Apellido del usuario') !!}
            {!! Form::Text('apellido',null,['class'=>'form-control','placeholder'=>'Ingrese su apellido.']) !!}
    </div>   
    <div class="form-group">
            {!! Form::label('mail', 'Direccion de Mail') !!}
            {!! Form::Text('mail',null,['class'=>'form-control','placeholder'=>'Ingrese nuevo mail']) !!}
    </div> 
    <div class="form-group">
            {!! Form::label('username ', 'Nombre de usuario ') !!}
            {!! Form::Text('username',null,['class'=>'form-control', 'placeholder'=>'Ingrese nuevo username']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('tipo_usuario_id', 'Sección    :') !!}
        {!!Form::select('tipo_usuario_id', $tiposuser, ['class' => 'form-control'])!!}
    </div>
            {!!Form::submit('Actualizar Usuario',['class'=>'btn btn-primary'])!!}
{!! Form::close() !!}
</div>
@endsection