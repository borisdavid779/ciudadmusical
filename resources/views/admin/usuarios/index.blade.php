@extends('layouts.app')

@section('title', 'Usuarios')

@section('content')
@include('alerts.errors')
@include('alerts.success')
@include('alerts.request')
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<div class="wrapper wrapper-content animated fadeInRight">
    <a class="btn btn-primary" href="{{ROUTE('usuarios.create')}}">NUEVO USUARIO</a>
    @if(count($usuarios))
    <table id="mytable" class="table table-striped table-bordered" style="width:100%" >
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRES</th>
                <th>TIPO USUARIO</th>
                 <th>APELLIDOS</th>
                 <th>USERNAME</th>
                 <th>MAIL</th>
                 <th>OPCIONES</th>
            </tr>
        </thead>
        <tbody>
            @foreach($usuarios as $usuario)
            <tr>
                <td>{{$usuario->id}}</td>
                <td>{{$usuario->nombre}}</td>
                <td>{{$usuario->tipo_usuario_id}}</td>
                <td>{{$usuario->apellido}}</td>
                <td>{{$usuario->username}}</td>
                <td>{{$usuario->mail}}</td>
                <td>
                 {!! Form::open(array('method' => 'GET', 'route' => array('usuarios.edit', $usuario->id))) !!}   
                      <button type="submit" class="btn btn-success bnt-lg"   data-toggle="tooltip"  title="Editar">
                      <i class="fa fa-pencil-square-o"></i></button>
                      {!! Form::close() !!}
         
       
                 {!! Form::open(array('method' => 'DELETE', 'route' => array('usuarios.destroy', $usuario->id))) !!}   
                      <button type="submit" class="btn btn-danger bnt-lg"   data-toggle="tooltip"  title="Eliminar">
                      <i class="fa fa-trash"></i></button>
                      {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endif
</div>
@endsection
@section('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      
            $('#mytable').DataTable({
                responsive : true,
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                }
              
                  
            });

} );
</script>
@stop
