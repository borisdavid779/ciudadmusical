<?php

namespace App\Http\Controllers\Auth;

use App\usuarioModel;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => 'required|max:255',
            'mail' => 'required|email|max:255|unique:usuario',
            'apellido' => 'required|apellido|max:255|unique:usuario',
            'username' => 'required|username|max:255|unique:usuario',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    protected function postRegister(Request $request)
    {
    $this->validate($request, [
        'nombre' => 'required',
        'mail' => 'required',
        'password' => 'required',
        'username' => 'required',
    ]);

    $data = $request;

    $user=new User;
    $user->nombre=$data['nombre'];
    $user->mail=$data['mail'];
    $user->password=bcrypt($data['password']);
    $user->username=$data['username'];

    if($user->save()){

         return "se ha registrado correctamente el usuario";
               
    }
}

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'mail' => $data['mail'],
            'password' => bcrypt($data['password']),
            'username' => $data['username'],
        ]);
    }
}
