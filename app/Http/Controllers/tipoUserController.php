<?php

namespace App\Http\Controllers;

use App\tipoUserModel;
use Illuminate\Http\Request;
use View;
use DB;
use Redirect;
use Session;
use Auth;
class tipoUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::user()->tipo_usuario_id==1){
            $tipos=tipoUserModel::all();
            //dd($tipo_usuario);
            return View::make('admin.tipousuarios.index',compact('tipos'));    
        }
        else{
            return dd("Necesitas ser cantante para ver los generos");
        }
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tipos=DB::table('tipo_usuario')->pluck('tipo', 'id', 'descripcion');
        return View::make('admin.tipousuarios.index', compact('tipos'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\cancionModel  $cancionModel
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cancionModel  $cancionModel
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\cancionModel  $cancionModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $requestl)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cancionModel  $cancionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy( )
    {
        //
    }
}
