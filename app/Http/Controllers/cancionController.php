<?php

namespace App\Http\Controllers;

use App\cancionModel;
use Illuminate\Http\Request;
use View;
use DB;
use Redirect;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Response;

class cancionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       // $canciones=cancionModel::all();
        //$events = Event::with('cancion')->all();

         $canciones=cancionModel::join('usuario','usuario.id','=','cancion.cantante_id')
         ->join('genero','genero.id','=','cancion.genero_id')
         ->get(['cancion.id','cancion.titulo','usuario.nombre'
            ,'usuario.apellido','cancion.descripcion','genero.genero', 'cancion.imagen_cancion', 'cancion.archivo']);


         if(Auth::user()->tipo_usuario_id==1)
         {
            $canciones=cancionModel::join('usuario','usuario.id','=','cancion.cantante_id')
             ->join('genero','genero.id','=','cancion.genero_id')
             ->get(['cancion.id','cancion.titulo','usuario.nombre'
                ,'usuario.apellido','cancion.descripcion','genero.genero','cancion.imagen_cancion', 'cancion.archivo']);
         }else
             if(Auth::user()->tipo_usuario_id==2)
             {
                $canciones=cancionModel::join('usuario','usuario.id','=','cancion.cantante_id')
                 ->join('genero','genero.id','=','cancion.genero_id')
                 ->where('cancion.cantante_id',Auth::id())
                 ->get(['cancion.id','cancion.titulo','cancion.descripcion','genero.genero','cancion.imagen_cancion', 'cancion.archivo']);
             }else{
                    //return dd("Necesitas ser cantante para subir canciones");
                    $canciones=cancionModel::join('usuario','usuario.id','=','cancion.cantante_id')
                    ->join('genero','genero.id','=','cancion.genero_id')
                    ->get(['cancion.id','cancion.titulo','usuario.nombre'
                    ,'usuario.apellido','cancion.descripcion','genero.genero','cancion.imagen_cancion', 'cancion.archivo']);
                  }
              //  dd($canciones);
        return View::make('admin.canciones.index',compact('canciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $generocancion=DB::table('genero')->pluck('genero', 'id');
        $artista=DB::table('usuario')
        ->select(DB::raw('CONCAT(apellido," ", nombre) AS nombre'),'id')
        ->pluck('nombre', 'id');
        return View::make('admin.canciones.subir', compact('generocancion', 'artista'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());

    header ("Connection: close");
        //dd(Auth::id());
        //
        //dd($request->all());
         $this->validate($request, [
            //'id' => 'required|unique:cancion',   
            'titulo' => 'required',     
            //'descripcion' => 'required',  
            //'nombre' => 'required|unique:genero',
            //'cantante_id' => 'required|unique:usuario',               
            ]
        );    
        try {

            $nueva_cancion=new cancionModel();
            $nueva_cancion->titulo=$request->titulo;
            $nueva_cancion->genero_id=$request->genero_id;
            $nueva_cancion->descripcion=$request->descripcion;

            if(Auth::user()->tipo_usuario_id==1)
            {

            $nueva_cancion->cantante_id=$request->cantante_id;                
            }else{
             $nueva_cancion->cantante_id=Auth::id();
            }
            //$nueva_cancion->cantante_id=Auth::id();
            //$nueva_cancion->nombre=$request->nombre;
            $nueva_cancion->save(); 

            //recogiendo imagen de cancion
            if ($request->cancion) 
            {
                $cancion = $request->file('imagen_cancion');
                $extensionCancion = $cancion->getClientOriginalExtension();
                $almacen_img = $nueva_cancion->id.'.'.$extensionCancion;
                $location_img = public_path(). '/imagenes_canciones/';
                $cancion->move($location_img, $almacen_img);
                //Se la paso al campo de la base de datos "imagen_cancion"
                $nueva_cancion->imagen_cancion='imagenes_canciones/'.$almacen_img;
                $nueva_cancion->save();
            }

            if ($request->cancion)
            {
                //guardo cancion en variable
                $cancion = $request->file('cancion');
                 //tomo la extension
                $extension=$cancion->getClientOriginalExtension(); 
                //creo el nombre como se guardar en directorio, el id mas la extension ejm 1.mp3
                $nombre_guardar=$nueva_cancion->id.'.'.$extension;
                //establezco donde se guardarn mis archivos
                $location = public_path(). '/canciones/';
                //digo donde se guarda, con que nombre y el "->move" hace el guardado
                $cancion->move($location, $nombre_guardar);
                //en el campo de la base de datos guardo la direccion donde se guardo
                $nueva_cancion->archivo='canciones/'.$nombre_guardar;
                //actualizo el registro
                $nueva_cancion->save();
            }
        
            Session::flash('message','Cancion añadida correctamente.');
        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('message-error','No se pudo añadir la cancion'.$e);
        }
        return Redirect::to('canciones');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\cancionModel  $cancionModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cancionModel  $cancionModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->tipo_usuario_id==1 or Auth::user()->tipo_usuario_id==2){
        $cancion = cancionModel::find($id);
        $generocancion=DB::table('genero')->pluck('genero', 'id');
        $artista=DB::table('usuario')->pluck('nombre','id');
        //}
        return View::make('admin.canciones.editar', compact('cancion', 'generocancion', 'artista'));
        //dd($cancion);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\cancionModel  $cancionModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
          try {
            $nueva_cancion= cancionModel::find($id);
            $nueva_cancion->titulo=$request->titulo;
            $nueva_cancion->genero_id=$request->genero_id;
            $nueva_cancion->descripcion=$request->descripcion;
            if(Auth::user()->tipo_usuario_id==1){
                $nueva_cancion->cantante_id=$request->cantante_id;
            }
            $nueva_cancion->save();
            Session::flash('message','Cancion actualizada correctamente.');
        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('message-error','No se pudo actualizar cancion');
        }
        return Redirect::to('canciones');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cancionModel  $cancionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        //
        if(Auth::user()->tipo_usuario_id==1 or Auth::user()->tipo_usuario_id==2){
        cancionModel::find($id)->delete();
        Session::flash('message','Cancion eliminada correctamente.');
        return Redirect::to('canciones');
        }
    }

    public function obtenercanciones()
    {
        $resultado= cancionModel::all();
        return json_encode($resultado);
    }

    public function obtenercancionesporgenero($genero)
    {
        $resultado= cancionModel::join('genero','genero.id','=','cancion.genero_id')
        ->where('genero.genero',$genero)
        ->get();
        return json_encode($resultado);
    }
    public function obtenercancionesporartista($usuario)
    {
        $resultado= cancionModel::join('usuario','usuario.id','=','cancion.cantante_id')
        ->where('usuario.nombre',$usuario)
        ->get();
        return json_encode($resultado);
    }
    public function info_solicitud($id)
    {
        $app_cabecera=appModel::find($id);
        $app_detalle=appdetalleModel::where('app_cabecera_id','like','%'.$id.'%')->get();
        return json_encode($app_detalle);
    }
    public function busquedamovilcancion($id)
    {
        $cancion=cancionModel::find($id);
        $cancion_detalle=cancionModel::where('titulo','like','%'.$descripcion.'%')->get();
        return json_encode($cancion_detalle);
    }
    public function scopeName($query, $name)
    {
        return $query->where('titulo', $name);
    }
}
     