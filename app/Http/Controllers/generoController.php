<?php

namespace App\Http\Controllers;

use App\generoModel;
use Illuminate\Http\Request;
use View;
use DB;
use Redirect;
use Session;
class generoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //if(Auth::user()->tipo_usuario_id==1)
        //{
            $generos=generoModel::all();
            return View::make('admin.generos.index',compact('generos'));
            //dd($generos);
        //}else{
          //  return dd("Necesitas ser cantante");   
        //}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $generosidit=DB::table('genero')->pluck('genero', 'id');
        return View::make('admin.generos.crear', compact('generosidit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
            $this->validate($request, [
            'genero' => 'required|unique:genero',        
            ]
          
          );
        try {
            $nuevo_genero=new generoModel();
            $nuevo_genero->genero=$request->genero;
            $nuevo_genero->save();
            Session::flash('message','Genero añadido correctamente.');
        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('message-error','No se pudo añadido genero'.$e);
        }
        return Redirect::to('generos');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\cancionModel  $cancionModel
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cancionModel  $cancionModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $genero = generoModel::find($id);
        return View::make('admin.generos.editar', compact('genero'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\cancionModel  $cancionModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            $nuevo_genero= generoModel::find($id);
            $nuevo_genero->genero=$request->genero;
            $nuevo_genero->save();
            Session::flash('message','Genero actualizado correctamente');
        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('message-error','No se pudo actualizar genero'.$e);
        }
        return Redirect::to('generos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cancionModel  $cancionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
             generoModel::find($id)->delete();
            Session::flash('message','Genero eliminado correctamente.');
             }catch(\Illuminate\Database\QueryException $e){
                  Session::flash('message-error','No se pudo añadir el genero por que esta siendo utilizado');                
             }
        return Redirect::to('generos');
    }
    public function obtenergeneros()
    {
        $resultadogenero= generoModel::all();
        return json_encode($resultadogenero);
    }
}
