<?php

namespace App\Http\Controllers;

use App\usuarioModel;
use Illuminate\Http\Request;
use View;
use DB;
use Auth;
use Redirect;
use Session;
use Illuminate\Support\Facades\Input;
class usuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {                   
        if(Auth::user()->tipo_usuario_id==1)
        {
            $usuarios=usuarioModel::all();
            return View::make('admin.usuarios.index',compact('usuarios'));
        }
        else{
            return dd("No puedes tienes los permisos necesarios para acceder");
        }                                                                                               
    }                                               

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $tiposuser=DB::table('tipo_usuario')->pluck('tipo', 'id');
         return View::make('admin.usuarios.crear', compact('tiposuser'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        //dd($request->all());
        $this->validate($request, [
            'apellido' => 'required|unique:usuario',   
            'nombre' => 'required',     
            'username' => 'required|unique:usuario',  
            'mail' => 'required|unique:usuario',
            ]
          
          );
        try {
            $nuevo_usuario=new usuarioModel();
            $nuevo_usuario->nombre=$request->nombre;
            $nuevo_usuario->apellido=$request->apellido;
            $nuevo_usuario->username=$request->username;
            $nuevo_usuario->tipo_usuario_id=$request->tipo_usuario_id;
            $nuevo_usuario->mail=$request->mail;
            $nuevo_usuario->save();
            Session::flash('message','Usuario añadido correctamente.');
        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('message-error','No se pudo añadir el usuario'.$e);
        }
        return Redirect::to('usuarios');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\userModel  $userModel
     * @return \Illuminate\Http\Response
     */
    public function show(userModel $userModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\userModel  $userModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $usuario = usuarioModel::find($id);
        $tiposuser=DB::table('tipo_usuario')->pluck('tipo', 'id');
        return View::make('admin.usuarios.editar', compact('usuario', 'tiposuser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\userModel  $userModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            $nuevo_usuario= usuarioModel::find($id);
            $nuevo_usuario->nombre=$request->nombre;
            $nuevo_usuario->apellido=$request->apellido;
            $nuevo_usuario->username=$request->username;
            $nuevo_usuario->tipo_usuario_id=$request->tipo_usuario_id;
            $nuevo_usuario->mail=$request->mail;
            $nuevo_usuario->save();
            Session::flash('message','Usuario actualizado correctamente');
        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('message-error','No se pudo actualizar el usuario'.$e);
        }
        return Redirect::to('usuarios');


    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\userModel  $userModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        usuarioModel::find($id)->delete();
        Session::flash('message','Usuario eliminado correctamente.');
        return Redirect::to('usuarios');
        
    }

    public function obtenerartistas($tipo_usuario)
    {
        $resultado= usuarioModel::join('tipo_usuario','tipo_usuario.id','=','usuario.tipo_usuario_id')
        ->where('tipo_usuario.tipo',$tipo_usuario)
        ->get();
        return json_encode($resultado);
    }
}

