<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\usuarioModel;
use View;
use Auth;
use Session;
use Illuminate\Support\Facades\Hash;
use Redirect;
class loginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$usuarios=usuarioModel::all();
        //return View::make('admin.usuarios.index',compact('usuarios'));
      //  return View::make('views.usuarios.artista.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /*
    public function comprobarUser($password,$usuario)
    {
        $resultado=array('username'=>'0');
        $usuario_consult=UsuarioModel::where('username',$usuario)->orwhere('mail',$usuario)->select('id','username','password')->first();
        if (count($usuario_consult)>0) {   
            if (Hash::check($password, $usuario_consult->password))
            {
               $resultado=$usuario_consult;
               $resultado->password="";
            }else
            {
            $resultado=array('username'=>'ids');
            }
        }
        return $resultado;
    }
       $usuario_consult=UsuarioModel::where('username',$usuario)->orwhere('mail',$usuario)->select('id','username','password')->first();
        if (count($usuario_consult)>0) {   
            if (Hash::check($password1, $usuario_consult->password1))
            {
               $resultado=$usuario_consult;
               $resultado->password1="";
            }else
            {
            $resultado=array('username'=>'id');
            }
        }
    */
    public function comprobarlogin($password,$usuario)
    {
        $resultado=array('username'=>'0');
        $usuario_consult=usuarioModel::where('username',$usuario)->orwhere('mail',$usuario)->select('id','username','password')->first();
        if (count($usuario_consult)>0) {   
            if (Hash::check($password, $usuario_consult->password))
            {
               $resultado=$usuario_consult;
               $resultado->password="";
            }else
            {
            $resultado=array('username'=>'id');
            }
        }
        //dd($resultado);
        return $resultado;
    }
    public function Logout()
        //public function logout(Request $request)
    {
        $usuario= \Auth::user();
        //$this->Auth->logout();

        Session::flush();

        return redirect('/');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        if(Auth::attempt(['username'=>$request['username'], 'password'=>$request['password']]))
        {  
            return Redirect('inicio');
        }else
        {
            //dd("contraseña incorrecta");
            Session::flash('message-error','Usuario o contraseña incorrectos');            
            return Redirect::back();
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
