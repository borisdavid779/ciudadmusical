<?php

namespace App\Http\Controllers;

use App\usuarioModel;
use Illuminate\Http\Request;
use View;
use DB;
use Redirect;
use Session;
use Auth;

class registerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => 'required|max:255',
            'mail' => 'required|mail|max:255|unique:usuario',
            'apellido' => 'required|apellido|max:255|unique:usuario',
            'username' => 'required|username|max:255|unique:usuario',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    protected function postRegister(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'mail' => 'required|unique:usuario',
            'password' => 'required',
            'username' => 'required|unique:usuario',
            'apellido' => 'required',
            //'archivo_img' => 'required',
        ],
            [
              'mail.required'=>'El correo electronico es requerido',
              'username.required'=>'El nombre de usuario es requerido',
              'email.password'=>'La contraseña es requerida',
            ]
    );

        $data = $request;

        $usuario=new usuarioModel;
        $usuario->nombre=$data['nombre'];
        $usuario->mail=$data['mail'];
        $usuario->password=bcrypt($data['password']);
        $usuario->username=$data['username'];
        $usuario->tipo_usuario_id=$request->tipo_usuario_id=2;
        $usuario->apellido=$data['apellido'];
        /*
            if ($request->cancion) 
            {
                $usuario = $request->file('archivo_img');
                $extension = $usuario->getClientOriginalExtension();
                $almacen_img = $usuario->id.'.'.$extension;
                $location_img = public_path(). '/imagenes_user/';
                $usuario->move($location_img, $almacen_img);
                //Se la paso al campo de la base de datos "imagen_cancion"
                $usuario->archivo_img='imagenes_user/'.$almacen_img;
                $usuario->save();
            }
        */
            if($usuario->save()){
                //dd($request->all());
                return Redirect('/');    
            }
    }
    //Registro usuario oyente
     protected function postregisterOyente(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'mail' => 'required',
            'password' => 'required',
            'username' => 'required',
            'apellido' => 'required',
    ]);

        $data = $request;

        $usuario=new usuarioModel;
        $usuario->nombre=$data['nombre'];
        $usuario->mail=$data['mail'];
        $usuario->password=bcrypt($data['password']);
        $usuario->username=$data['username'];
        $usuario->tipo_usuario_id=$request->tipo_usuario_id=3;
        $usuario->apellido=$data['apellido'];

            if($usuario->save()){
                //dd($request->all());
                return Redirect('/');    
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$tiposuser=DB::table('tipo_usuario')->pluck('tipo', 'id');
         //return View::make('admin.usuarios.crear', compact('tiposuser'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
